#pragma once
#include "ITreeNodes.h"
#include "CIdentifier.h"
#include "CType.h"


namespace AST
{

class CArgument : public ITreeNode
{

 public:
    explicit CArgument( CType* t, CIdentifier* s ) : type( t ), id( s )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CType* GetType() const
    {
        return type.get();
    }

    const CIdentifier* GetId() const
    {
        return id.get();
    }

 private:
    std::unique_ptr<CType> type;
    std::unique_ptr<CIdentifier> id;
};

}
