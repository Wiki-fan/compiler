#include "CClassDeclaration.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CClassDeclaration::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

}