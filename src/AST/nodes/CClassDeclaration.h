#pragma once
#include "ITreeNodes.h"
#include "CMethodDeclaration.h"


namespace AST
{

class CClassDeclaration : public ITreeNode
{

 public:
    explicit CClassDeclaration( CIdentifier* name,
                                CIdentifier* extendsName,
                                std::vector<std::unique_ptr<CVarDeclaration>>* vars,
                                std::vector<std::unique_ptr<CMethodDeclaration>>* methods ) :
        id( name ), extendsId( extendsName ), varDeclarations( vars ), methodDeclarations( methods )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CIdentifier* GetId() const
    {
        return id.get();
    }

    const CIdentifier* GetExtend() const
    {
        return extendsId.get();
    }

    const std::vector<std::unique_ptr<CVarDeclaration>>* GetVarDeclarations() const
    {
        return varDeclarations.get();
    }

    const std::vector<std::unique_ptr<CMethodDeclaration>>* GetMethodDeclarations() const
    {
        return methodDeclarations.get();
    }

 private:
    std::unique_ptr<CIdentifier> id;
    std::unique_ptr<CIdentifier> extendsId;
    std::unique_ptr<std::vector<std::unique_ptr<CVarDeclaration>>> varDeclarations;
    std::unique_ptr<std::vector<std::unique_ptr<CMethodDeclaration>>> methodDeclarations;
};

}
