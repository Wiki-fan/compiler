#include "ITreeNodes.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CGoal::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

const CMainClass* CGoal::GetMainClass() const
{
    return mainClass.get();
}

const std::vector<std::unique_ptr<CClassDeclaration>>* CGoal::GetClassDeclarations() const
{
    return classDeclarations.get();
}

}