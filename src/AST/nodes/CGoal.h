#pragma once
#include "ITreeNodes.h"
#include "CMainClass.h"
#include "CClassDeclaration.h"


namespace AST
{

class CGoal : public ITreeNode
{

 public:
    CGoal( CMainClass* _mainClass, std::vector<std::unique_ptr<CClassDeclaration>>* _classDeclarations ) :
        mainClass( _mainClass ), classDeclarations( _classDeclarations )
    {
    }

    void Accept( IVisitor* v ) const override;
    const CMainClass* GetMainClass() const;
    const std::vector<std::unique_ptr<CClassDeclaration>>* GetClassDeclarations() const;

 private:
    std::unique_ptr<CMainClass> mainClass;
    std::unique_ptr<std::vector<std::unique_ptr<CClassDeclaration>>> classDeclarations;
};

}
