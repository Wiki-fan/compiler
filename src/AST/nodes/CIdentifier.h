#pragma once

#include "ITreeNodes.h"


namespace AST
{

class CIdentifier : public ITreeNode
{

 public:
    explicit CIdentifier( CSymbol value_ ) : value( value_ )
    {
    }

    void Accept( IVisitor* v ) const override;
    const CSymbol& GetValue() const;

 private:
    CSymbol value;
};

}
