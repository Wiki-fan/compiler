#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CMainClass::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

const CIdentifier* CMainClass::GetId() const
{
    return id.get();
}

const CIdentifier* CMainClass::GetArgsId() const
{
    return argsId.get();
}

const CIdentifier* CMainClass::GetMainId() const
{
    return mainId.get();
}

const IStm* CMainClass::GetStatement() const
{
    return statement.get();
}
}
