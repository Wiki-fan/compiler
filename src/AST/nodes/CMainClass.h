#pragma once

#include "ITreeNodes.h"
#include "CIdentifier.h"


namespace AST
{

class CMainClass : public ITreeNode
{

 public:
    explicit CMainClass( CIdentifier* s, CIdentifier* args, IStm* stmt, CIdentifier* mainId_ ) :
        id( s ), argsId( args ), statement( stmt ), mainId( mainId_ )
    {
    }

    void Accept( IVisitor* v ) const override;
    const CIdentifier* GetId() const;
    const CIdentifier* GetArgsId() const;
    const CIdentifier* GetMainId() const;
    const IStm* GetStatement() const;

 private:
    std::unique_ptr<CIdentifier> id;
    std::unique_ptr<CIdentifier> argsId;
    std::unique_ptr<CIdentifier> mainId;
    std::unique_ptr<IStm> statement;
};

}
