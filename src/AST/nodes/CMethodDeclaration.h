#pragma once
#include "ITreeNodes.h"
#include "CVarDeclaration.h"
#include "CArgument.h"
#include <memory>
#include <vector>


namespace AST
{

class CMethodDeclaration : public ITreeNode
{

 public:
    explicit CMethodDeclaration( CType* t, CIdentifier* s, std::vector<std::unique_ptr<CArgument>>* args,
                                 std::vector<std::unique_ptr<CVarDeclaration>>* localArgs,
                                 std::vector<std::unique_ptr<IStm>>* states,
                                 IExp* returnExpr ) :
        type( t ), id( s ), arguments( args ), localArguments( localArgs ),
        statements( states ), returnExp( returnExpr )
    {
    }

    void Accept( IVisitor* v ) const override;
    CType* GetType() const;
    const CIdentifier* GetId() const;
    const std::vector<std::unique_ptr<CArgument>>* GetArguments() const;
    const std::vector<std::unique_ptr<CVarDeclaration>>* GetLocalArguments() const;
    const std::vector<std::unique_ptr<IStm>>* GetStatements() const;
    const IExp* GetReturnExp() const;

 private:
    std::unique_ptr<CType> type;
    std::unique_ptr<CIdentifier> id;
    std::unique_ptr<std::vector<std::unique_ptr<CArgument>>> arguments;
    std::unique_ptr<std::vector<std::unique_ptr<CVarDeclaration>>> localArguments;
    std::unique_ptr<std::vector<std::unique_ptr<IStm>>> statements;
    std::unique_ptr<IExp> returnExp;
};

}
