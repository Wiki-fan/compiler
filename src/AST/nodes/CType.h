#pragma once

#include <cassert>
#include "ITreeNodes.h"
#include "CIdentifier.h"


namespace AST
{

class CType : public ITreeNode
{

 public:
    enum class TYPE
    {
        INT_ARR, INT, BOOLEAN, CUSTOM, UNKNOWN, VOID, STR_ARR
    };

    explicit CType( TYPE type_, CIdentifier* name_ = nullptr ) : type( type_ ), name( name_ )
    {
    }

    CType( const CType& other ) : ITreeNode( other ), type( other.type ), name( other.name.get() )
    {
    }

    CType& operator=( const CType& other );
    bool operator==( const CType& other ) const;
    bool operator!=( const CType& other ) const;

    void Accept( IVisitor* v ) const override;
    TYPE GetType() const;
    const CIdentifier* GetName() const;
    const std::string GetString() const;

 private:
    TYPE type;
    std::unique_ptr<CIdentifier> name;
};

}
