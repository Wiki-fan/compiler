#include "CVarDeclaration.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CVarDeclaration::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

const CType* CVarDeclaration::GetType() const
{
    return type.get();
}

const CIdentifier* CVarDeclaration::GetId() const
{
    return id.get();
}

}
