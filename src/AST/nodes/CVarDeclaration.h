#pragma once
#include "ITreeNodes.h"
#include "CIdentifier.h"
#include "CType.h"


namespace AST
{

class CVarDeclaration : public ITreeNode
{

 public:
    explicit CVarDeclaration( CType* t, CIdentifier* s ) : type( t ), id( s )
    {
    }

    void Accept( IVisitor* v ) const override;
    const CType* GetType() const;
    const CIdentifier* GetId() const;

 private:
    std::unique_ptr<CType> type;
    std::unique_ptr<CIdentifier> id;
};

}
