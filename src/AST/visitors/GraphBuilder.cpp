#include <src/common.h>
#include "GraphBuilder.h"


namespace AST
{

CGraphBuilder::CGraphBuilder( FILE* output ) :
    nodeNumber( 0 )
{
    f = output;
    fprintf( f, "%s", "strict graph G{\n" );
}

CGraphBuilder::~CGraphBuilder()
{
    fprintf( f, "%s", "}" );
    fclose( f );
}

void CGraphBuilder::Visit( const CArgument* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CArgument\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetType()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
}

void CGraphBuilder::Visit( const CGoal* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CGoal\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetMainClass()->Accept( this );
    for( int i = 0; i < n->GetClassDeclarations()->size(); i++ ) {
        auto& ptr = ( *n->GetClassDeclarations() )[i];
        if( ptr ) {
            nodeNumber++;
            fprintf( f, "%d -- %d;\n", cur, nodeNumber );
            ptr->Accept( this );
        } else {
            throw CompilerException( "Trying to Visit nullptr" );
        }
    }
}

void CGraphBuilder::Visit( const CMainClass* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CMainClass\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetArgsId()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetStatement()->Accept( this );
}

void CGraphBuilder::Visit( const CClassDeclaration* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CClassDeclaration\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
    if( n->GetExtend() ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        n->GetExtend()->Accept( this );
    }
    for( int i = 0; i < n->GetVarDeclarations()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetVarDeclarations() ) )[i]->Accept( this );
    }
    for( int i = 0; i < n->GetMethodDeclarations()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetMethodDeclarations() ) )[i]->Accept( this );
    }
}

void CGraphBuilder::Visit( const CVarDeclaration* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CVarDeclaration\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetType()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
}

void CGraphBuilder::Visit( const CMethodDeclaration* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CMethodDeclaration\\n%s\"];\n", cur, n->GetId()->GetValue()->String().c_str() );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetType()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
    for( int i = 0; i < n->GetArguments()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetArguments() ) )[i]->Accept( this );
        if( i < n->GetArguments()->size() - 1 ) {
        }
    }
    for( int i = 0; i < n->GetLocalArguments()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetLocalArguments() ) )[i]->Accept( this );
    }
    for( int i = 0; i < n->GetStatements()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetStatements() ) )[i]->Accept( this );
    }
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetReturnExp()->Accept( this );
}

void CGraphBuilder::Visit( const CType* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CType\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d [label=\"%s\"];\n", nodeNumber, n->GetString().c_str() );
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
}

void CGraphBuilder::Visit( const CIfStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CIfStm\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetIfStm()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetElseStm()->Accept( this );
}

void CGraphBuilder::Visit( const CWhileStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CWhileStm\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetStm()->Accept( this );
}

void CGraphBuilder::Visit( const CComplexStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CComplexStm\"];\n", cur );
    for( int i = 0; i < n->GetStatements()->size(); i++ ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *( n->GetStatements() ) )[i]->Accept( this );
    }
}

void CGraphBuilder::Visit( const CPrintStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CPrintStm\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
}

void CGraphBuilder::Visit( const CAssignmentStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CAssignmentStm\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
}

void CGraphBuilder::Visit( const CArrayAssignmentStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CArrayAssignmentStm\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetId()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetIndex()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetResult()->Accept( this );
}

void CGraphBuilder::Visit( const CBinaryExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"%s\"];\n", cur, n->GetTypeStr().c_str() );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetLeft()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetRight()->Accept( this );
}

void CGraphBuilder::Visit( const CIndexExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CIndexExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetBase()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetIndex()->Accept( this );
}

void CGraphBuilder::Visit( const CLengthExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CLengthExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
}

void CGraphBuilder::Visit( const CMethodCallExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CMethodCallExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetCaller()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetFuncName()->Accept( this );
    for( int i = 0; i < n->GetArgs()->size(); ++i ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        ( *n->GetArgs() )[i]->Accept( this );
        if( i != n->GetArgs()->size() - 1 ) {
        }
    }
}

void CGraphBuilder::Visit( const CIntegerExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CIntegerExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    fprintf( f, "%d [label=\"%d\"];\n", nodeNumber, n->GetValue() );
}

void CGraphBuilder::Visit( const CBooleanConstExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CBooleanConstExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    fprintf( f, "%d [label=\"%s\"];\n", nodeNumber, n->GetValue() ? "true" : "false" );

}

void CGraphBuilder::Visit( const CIdentifierExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CIdentifierExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetValue()->Accept( this );
}

void CGraphBuilder::Visit( const CThisExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CThisExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    fprintf( f, "%d [label=\"this\"];\n", nodeNumber );

}

void CGraphBuilder::Visit( const CNewArrExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CNewArrExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetLength()->Accept( this );
}

void CGraphBuilder::Visit( const CNewExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CNewExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetType()->Accept( this );
}

void CGraphBuilder::Visit( const CNegationExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CNegationExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetOp()->Accept( this );
}

void CGraphBuilder::Visit( const CParenthesesExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CParenthesesExp\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
}

void CGraphBuilder::Visit( const CIdentifier* n )
{
    fprintf( f, "%d [label=\"%s\"];\n", nodeNumber, n->GetValue()->String().c_str() );
}

}
