#pragma once

#include "IAddress.h"
#include <src/IRT/nodes/CIRTExp.h>


namespace IRT
{

class CInRegAddress : public IAddress
{

 public:
    explicit CInRegAddress( const CTemp& temp_ ) : temp( temp_ )
    {
    }

    IExp* ToExp() const override;

 private:
    CTemp temp;
};

}
