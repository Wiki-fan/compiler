#pragma once

#include <src/IRT/nodes/CIRTExp.h>
#include "IAddress.h"
#include "../SymbolTable/CSymbol.h"
#include "CInRegAddress.h"
#include "CInFrameAddress.h"


namespace IRT
{

class IFrame
{

 public:
    virtual ~IFrame() = default;

    virtual const std::string& GetName() const = 0;
    virtual int WordSize() const = 0;
    virtual void AddFieldAddress( const std::string& name ) = 0;
    virtual void AddLocalAddress( const std::string& name ) = 0;
    virtual void AddAddress( const std::string& name, const IAddress* address ) = 0;
    virtual const IAddress* GetAddress( const std::string& name ) = 0;
    virtual const IExp* ExternalCall( const std::string& functionName, const IExp* arg ) const = 0;
};

}
