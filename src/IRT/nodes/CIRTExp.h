#pragma once

#include <memory>
#include <map>
#include "IExp.h"
#include "IStm.h"
#include "CLabel.h"
#include "List.h"


namespace IRT
{
class CConstExp : public IExp
{

 public:

    explicit CConstExp( int value ) : value( value )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    int GetValue() const;

 private:
    const int value;
};

class CNameExp : public IExp
{

 public:

    explicit CNameExp( const CLabel& label ) : label( label )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const CLabel& GetLabel() const;

 private:
    CLabel label;
};

class CTempExp : public IExp
{

 public:
    explicit CTempExp( const CTemp& value_ ) : value( value_ )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const std::string GetValueLabel() const;

 private:
    CTemp value;
};

class CBinaryExp : public IExp
{

 public:
    enum class EBinaryType
    {
        AND,
        PLUS,
        MINUS,
        MULTIPLY
    };

    CBinaryExp( EBinaryType binType, const IExp* left, const IExp* right ) :
        binType( binType ), leftExp( left ), rightExp( right )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const std::string& GetTypeStr() const;
    const IExp* GetLeft() const;
    const IExp* GetRight() const;

 private:
    const static std::map<EBinaryType, const std::string> TypeToStr;
    const EBinaryType binType;
    std::unique_ptr<const IExp> leftExp;
    std::unique_ptr<const IExp> rightExp;
};

class CMemoryExp : public IExp
{

 public:

    explicit CMemoryExp( const IExp* exp );

    void Accept( IVisitorIRT* v ) const override;
    const IExp* GetMem() const;

 private:
    std::unique_ptr<const IExp> exp;
};

class CCallExp : public IExp
{

 public:

    CCallExp( const IExp* funcExp, const CExpList* args ) : funcExp( funcExp ), args( args )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const IExp* GetFuncExp() const;
    const CExpList* GetArgs() const;

 private:
    std::unique_ptr<const IExp> funcExp;
    std::unique_ptr<const CExpList> args;
};

class CESeqExp : public IExp
{

 public:

    CESeqExp( const IStm* stm, const IExp* exp ) : stm( stm ), exp( exp )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const IStm* GetStm() const;
    const IExp* GetExp() const;

 private:
    std::unique_ptr<const IStm> stm;
    std::unique_ptr<const IExp> exp;
};
}
