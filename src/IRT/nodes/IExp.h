#pragma once

namespace IRT
{

class IVisitorIRT;

class IExp
{

 public:
    IExp() = default;
    virtual ~IExp() = default;

    virtual void Accept( IVisitorIRT* v ) const = 0;
};

}
