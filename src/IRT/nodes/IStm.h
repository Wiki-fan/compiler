#pragma once

namespace IRT
{

class IVisitorIRT;

class IStm
{

 public:
    IStm() = default;
    virtual ~IStm() = default;

    virtual void Accept( IVisitorIRT* v ) const = 0;
};

}
