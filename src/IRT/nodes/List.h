#pragma once

#include <vector>
#include <memory>
#include "IExp.h"
#include "IStm.h"


namespace IRT
{

class CExpList
{

 public:
    CExpList() = default;

    explicit CExpList( const IExp* expression )
    {
        Add( expression );
    }

    void Add( const IExp* expression )
    {
        expressions.emplace_back( expression );
    }

    const std::vector<std::unique_ptr<const IExp>>& GetExpressions() const
    {
        return expressions;
    }

 private:
    std::vector<std::unique_ptr<const IExp>> expressions;
};

}
