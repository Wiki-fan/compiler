#include <src/SymbolTable/CTypeInfo.h>
#include "CTranslator.h"
#include "src/IRT/nodes/CIRTExp.h"
#include "src/IRT/ISubtreeWrapper.h"


namespace IRT
{

void CTranslator::AddClassFields( const CClassInfo* classDefinition )
{
    if( classDefinition->GetParent() != CSymbol::None ) {
        AddClassFields( classDefinition->GetParentInfo() );
    }
    for( auto& it: classDefinition->GetFields() ) {
        curFrame->AddFieldAddress( it.first->String() );
    }
}

void CTranslator::BuildNewFrame( CSymbol methodSymbol )
{
    CClassInfo* classDefinition = symbolTable->classes[curClass->GetName()].get();
    CMethodInfo* methodDefinition = classDefinition->GetMethod( methodSymbol );

    curFrame = new CFrame( classDefinition->GetName(), methodDefinition->GetName() );

    AddClassFields( classDefinition );

    for( auto& it: methodDefinition->GetArgs() ) {
        curFrame->AddLocalAddress( it.first->String() );
    }
    for( auto& it: methodDefinition->GetLocals() ) {
        curFrame->AddLocalAddress( it.first->String() );
    }
}

void CTranslator::ProcessStmList( const std::vector<std::unique_ptr<AST::IStm>>* statements )
{

    std::unique_ptr<ISubtreeWrapper> rightTail = nullptr;

    if( !statements->empty() ) {
        statements->back()->Accept( this );
        rightTail = std::move( curWrapper );
        for( auto stmt = std::next( statements->crbegin() ); stmt != statements->crend(); ++stmt ) {
            ( *stmt )->Accept( this );
            std::unique_ptr<ISubtreeWrapper> curResult = std::move( curWrapper );
            rightTail =
                std::unique_ptr<ISubtreeWrapper>(
                    new CStmtWrapper(
                        new CSeqStm(
                            curResult->ToStm(),
                            rightTail->ToStm()
                        )
                    )
                );
        }
    }
    curWrapper = std::move( rightTail );
}

CBinaryExp::EBinaryType CTranslator::BinaryOperatorFromAstToIr( AST::CBinaryExp::TYPE t )
{
    switch( t ) {
        case AST::CBinaryExp::TYPE::PLUS:
            return CBinaryExp::EBinaryType::PLUS;
        case AST::CBinaryExp::TYPE::MINUS:
            return CBinaryExp::EBinaryType::MINUS;
        case AST::CBinaryExp::TYPE::MUL:
            return CBinaryExp::EBinaryType::MULTIPLY;
        default:
            assert( false );
    }
}

void CTranslator::Visit( const CGoal* n )
{
    // DONE
    n->GetMainClass()->Accept( this );

    for( const auto& clazz : *n->GetClassDeclarations() ) {
        clazz->Accept( this );
    }
}

void CTranslator::Visit( const CMainClass* n )
{
    // DONE
    // It is like function call
    curClass = symbolTable->classes[n->GetId()->GetValue()].get();
    BuildNewFrame( curClass->BeginMethodName() ); // In MiniJava it will always be "main"

    n->GetStatement()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> stmtWrapper = std::move( curWrapper );
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CStmtWrapper(
                new CSeqStm(
                    new CLabelStm( CLabel( curFrame->GetName() ) ),
                    stmtWrapper->ToStm()
                )
            )
        );

    CCodeFragment cf( curFrame, curWrapper->ToStm() );
    codeFragments.emplace( curFrame->GetName(), std::move( cf ) );
}

void CTranslator::Visit( const CClassDeclaration* n )
{
    // DONE
    curClass = symbolTable->classes[n->GetId()->GetValue()].get();
    for( auto& method:*n->GetMethodDeclarations() ) {
        method->Accept( this );
    }
    curClass = nullptr;
}

void CTranslator::Visit( const CVarDeclaration* n )
{
    // DONE
    assert( false );
}

void CTranslator::Visit( const CArgument* n )
{
    // DONE
    assert( false );
}

void CTranslator::Visit( const CMethodDeclaration* n )
{
    // DONE
    curMethod = curClass->GetMethod( n->GetId()->GetValue() );
    BuildNewFrame( n->GetId()->GetValue() );

    ProcessStmList( n->GetStatements() );
    std::unique_ptr<const ISubtreeWrapper> statementsWrapper = std::move( curWrapper );

    n->GetReturnExp()->Accept( this );
    const IExp* returnExpression = curWrapper->ToExp();

    if( statementsWrapper ) {
        curWrapper =
            std::unique_ptr<ISubtreeWrapper>(
                new CStmtWrapper(
                    new CSeqStm(
                        new CLabelStm(
                            CLabel( curFrame->GetName() )
                        ),
                        new CSeqStm(
                            statementsWrapper->ToStm(),
                            new CMoveStm(
                                curFrame->GetAddress( CFrame::RET )->ToExp(),
                                returnExpression
                            )
                        )
                    )
                )
            );
    } else {
        curWrapper =
            std::unique_ptr<ISubtreeWrapper>(
                new CStmtWrapper(
                    new CSeqStm(
                        new CLabelStm( CLabel( curFrame->GetName() ) ),
                        new CMoveStm(
                            curFrame->GetAddress( CFrame::RET )->ToExp(),
                            returnExpression
                        )
                    )
                )
            );
    }
    CCodeFragment cf( curFrame, curWrapper->ToStm() );
    codeFragments.emplace( curFrame->GetName(), std::move( cf ) );
    curMethod = nullptr;
}

void CTranslator::Visit( const CType* n )
{
    // DONE
    assert( false );
}

void CTranslator::Visit( const CIfStm* n )
{
    // DONE
    n->GetExp()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> condWrapper = std::move( curWrapper );

    n->GetIfStm()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> trueWrapper = std::move( curWrapper );

    n->GetElseStm()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> falseWrapper = std::move( curWrapper );

    CLabel labelTrue( "IFTRUE", true );
    CLabel labelFalse( "IFFALSE", true );
    CLabel labelJoin( "IFEXIT", true );
    auto resultLabelFalse = labelJoin;
    auto resultLabelTrue = labelJoin;

    IStm* suffix = new CLabelStm( labelJoin );
    if( falseWrapper ) {
        resultLabelFalse = labelFalse;

        suffix =
            new CSeqStm(
                new CLabelStm( labelFalse ),
                new CSeqStm(
                    falseWrapper->ToStm(),
                    suffix
                )
            );
        if( trueWrapper ) {
            suffix =
                new CSeqStm(
                    new CJumpStm( labelJoin ),
                    suffix
                );
        }
    }

    if( trueWrapper ) {
        resultLabelTrue = labelTrue;

        suffix =
            new CSeqStm(
                new CLabelStm( labelTrue ),
                new CSeqStm(
                    trueWrapper->ToStm(), suffix
                )
            );
    }

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CStmtWrapper(
                new CSeqStm(
                    condWrapper->ToConditional( resultLabelTrue, resultLabelFalse ),
                    suffix
                )
            )
        );
}

void CTranslator::Visit( const CWhileStm* n )
{
    // DONE
    n->GetExp()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> expWrapper = std::move( curWrapper );

    n->GetStm()->Accept( this );
    std::unique_ptr<const ISubtreeWrapper> stmWrapper = std::move( curWrapper );

    CLabel labelLoop( "WHILELOOP", true );
    CLabel labelBody( "WHILEBODY", true );
    CLabel labelDone( "WHILEDONE", true );

    IStm* suffix = new CSeqStm( new CJumpStm( labelLoop ),
                                new CLabelStm( labelDone ) );
    if( stmWrapper ) {
        suffix = new CSeqStm( stmWrapper->ToStm(), suffix );
    }

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CStmtWrapper(
                new CSeqStm(
                    new CLabelStm( labelLoop ),
                    new CSeqStm(
                        expWrapper->ToConditional( labelBody, labelDone ),
                        new CSeqStm(
                            new CLabelStm( labelBody ),
                            suffix
                        )
                    )
                )
            )
        );
}

void CTranslator::Visit( const CComplexStm* n )
{
    // DONE
    ProcessStmList( n->GetStatements() );
}

void CTranslator::Visit( const CPrintStm* n )
{
    // DONE
    n->GetExp()->Accept( this );

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                curFrame->ExternalCall( "print",
                                        curWrapper->ToExp()
                )
            )
        );
}

void CTranslator::Visit( const CAssignmentStm* n )
{
    // DONE
    n->GetExp()->Accept( this );
    const IExp* dst = curWrapper->ToExp();

    n->GetId()->Accept( this );
    const IExp* src = curWrapper->ToExp();

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CStmtWrapper(
                new CMoveStm( dst, src )
            )
        );
}

void CTranslator::Visit( const CArrayAssignmentStm* n )
{
    // DONE
    n->GetId()->Accept( this );
    auto leftExpr = curWrapper->ToExp();

    n->GetIndex()->Accept( this );
    auto indexExpr = curWrapper->ToExp();

    n->GetResult()->Accept( this );
    auto resultExpr = curWrapper->ToExp();

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CStmtWrapper(
                new CMoveStm(
                    resultExpr,
                    new CMemoryExp(
                        new CBinaryExp(
                            CBinaryExp::EBinaryType::PLUS,
                            leftExpr,
                            new CBinaryExp(
                                CBinaryExp::EBinaryType::MULTIPLY,
                                new CBinaryExp(
                                    CBinaryExp::EBinaryType::PLUS,
                                    indexExpr,
                                    new CConstExp( 1 )
                                ),
                                new CConstExp(
                                    curFrame->WordSize()
                                )
                            )
                        )
                    )
                )
            )
        );
}

void CTranslator::Visit( const AST::CBinaryExp* n )
{
    // DONE
    n->GetLeft()->Accept( this );
    auto leftWrapper = std::move( curWrapper );

    n->GetRight()->Accept( this );
    auto rightWrapper = std::move( curWrapper );

    if( n->GetType() == AST::CBinaryExp::TYPE::LESS ) {
        curWrapper =
            std::unique_ptr<ISubtreeWrapper>(
                new CRelationConditionalWrapper(
                    CCJumpStm::ERelationType::LT,
                    leftWrapper->ToExp(),
                    rightWrapper->ToExp()
                )
            );
    } else if( n->GetType() == AST::CBinaryExp::TYPE::AND ) {
        curWrapper =
            std::unique_ptr<ISubtreeWrapper>(
                new CAndConditionalWrapper(
                    leftWrapper.release(),
                    rightWrapper.release()
                )
            );
    } else {
        CBinaryExp::EBinaryType operatorType = BinaryOperatorFromAstToIr( n->GetType() );

        curWrapper =
            std::unique_ptr<ISubtreeWrapper>(
                new CExpWrapper(
                    new CBinaryExp(
                        operatorType,
                        leftWrapper->ToExp(),
                        rightWrapper->ToExp()
                    )
                )
            );
    }
}

void CTranslator::Visit( const CIndexExp* n )
{
    // DONE
    n->GetBase()->Accept( this );
    auto baseExpr = curWrapper->ToExp();

    n->GetIndex()->Accept( this );
    auto indexExpr = curWrapper->ToExp();

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                new CMemoryExp(
                    new CBinaryExp(
                        CBinaryExp::EBinaryType::PLUS,
                        baseExpr,
                        new CBinaryExp(
                            CBinaryExp::EBinaryType::MULTIPLY,
                            new CBinaryExp(
                                CBinaryExp::EBinaryType::PLUS,
                                indexExpr,
                                new CConstExp( 1 )
                            ),
                            new CConstExp( curFrame->WordSize() )
                        )
                    )
                )
            )
        );
}

void CTranslator::Visit( const CLengthExp* n )
{
    // Array length in the 1st element.
    n->GetExp()->Accept( this );
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                curWrapper->ToExp()
            )
        );
}

void CTranslator::Visit( const CMethodCallExp* n )
{
    // DONE
    n->GetCaller()->Accept( this );
    std::unique_ptr<ISubtreeWrapper> callerWrapper = std::move( curWrapper );

    // BEFORE ACCEPTING ARGUMENTS
    CClassInfo* callerClassInfo = symbolTable->classes[callerClassSymbol].get();
    const CMethodInfo* methodInfo = callerClassInfo->GetMethod( n->GetFuncName()->GetValue() );
    const CTypeInfo retType = CTypeInfo( *methodInfo->GetRetType() );
    if( retType.GetType() == CType::TYPE::CUSTOM ) {
        callerClassSymbol = retType.GetName();
    }

    auto listOfCallerAndArguments = new CExpList( callerWrapper->ToExp() );
    for( auto& arg : *n->GetArgs() ) {
        arg->Accept( this );
        listOfCallerAndArguments->Add( curWrapper->ToExp() );
    }
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                new CCallExp(
                    new CNameExp(
                        CLabel( MakeMethodFullName( callerClassSymbol->String(),
                                                    n->GetFuncName()->GetValue()->String() ) )
                    ),
                    listOfCallerAndArguments
                )
            )
        );
}

void CTranslator::Visit( const CIntegerExp* n )
{
    // DONE
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                new CConstExp( n->GetValue() )
            )
        );
}

void CTranslator::Visit( const CBooleanConstExp* n )
{
    // DONE
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                new CConstExp( n->GetValue() ? 1 : 0 )
            )
        );
}

void CTranslator::Visit( const CIdentifierExp* n )
{
    const IAddress* address = curFrame->GetAddress( n->GetValue()->GetValue()->String() );

    if( address ) {
        // expression is a name of local var / argument / field
        const CType* type;
        if( !curMethod->HasOwnVariableDeclared( n->GetValue()->GetValue() ) ) {

            // expression is a name of field
            curWrapper =
                std::unique_ptr<ISubtreeWrapper>(
                    new CExpWrapper(
                        address->ToExp()
                    )
                );
            type = curClass->GetField( n->GetValue()->GetValue() )->GetType();
        } else {
            // expression is a name of local var / argument
            curWrapper =
                std::unique_ptr<ISubtreeWrapper>(
                    new CExpWrapper(
                        address->ToExp()
                    )
                );
            type = curMethod->GetVarDeclared( n->GetValue()->GetValue() )->GetType();
        }

        if( type->GetType() == CType::TYPE::CUSTOM ) {
            callerClassSymbol = type->GetName()->GetValue();
        }
    } else {
        assert( false );
    }
}

void CTranslator::Visit( const CThisExp* n )
{
    // DONE
    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                curFrame->GetAddress( CFrame::THIS )->ToExp()
            )
        );
    callerClassSymbol = curClass->GetName();
}

void CTranslator::Visit( const CNewArrExp* n )
{
    // DONE
    n->GetLength()->Accept( this );
    auto lengthExpr = curWrapper->ToExp();

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                curFrame->ExternalCall(
                    "malloc",
                    new CBinaryExp(
                        CBinaryExp::EBinaryType::MULTIPLY,
                        new CBinaryExp(
                            CBinaryExp::EBinaryType::PLUS,
                            lengthExpr,
                            new CConstExp( 1 )
                        ),
                        new CConstExp(
                            curFrame->WordSize()
                        )
                    )
                )
            )
        );
}

void CTranslator::Visit( const CNewExp* n )
{
    // DONE
    const CClassInfo* curClassInfo = symbolTable->classes[n->GetType()->GetValue()].get();
    int fieldCount = curClassInfo->GetSize();

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CExpWrapper(
                curFrame->ExternalCall(
                    "malloc",
                    new CBinaryExp(
                        CBinaryExp::EBinaryType::MULTIPLY,
                        new CConstExp( fieldCount ),
                        new CConstExp( curFrame->WordSize() )
                    )
                )
            )
        );

    callerClassSymbol = n->GetType()->GetValue();

}

void CTranslator::Visit( const CNegationExp* n )
{
    // DONE
    n->GetOp()->Accept( this );

    curWrapper =
        std::unique_ptr<ISubtreeWrapper>(
            new CNegateConditionalWrapper(
                curWrapper.release()
            )
        );
}

void CTranslator::Visit( const CParenthesesExp* n )
{
    // DONE
    n->GetExp()->Accept( this );
    // keep curWrapper
}

void CTranslator::Visit( const CIdentifier* n )
{
    // DONE
    assert( false );
}

const std::map<std::string, CCodeFragment>& CTranslator::GetCodeFragments() const
{
    return codeFragments;
}

}
