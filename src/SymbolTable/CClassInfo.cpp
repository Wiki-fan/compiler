#include "CClassInfo.h"


CVariableInfo* CClassInfo::GetField( CSymbol s )
{
    if( fields.find( s ) != fields.end() ) {
        return fields[s].get();
    }

    if( parentInfo != nullptr ) {
        return parentInfo->GetField( s );
    }

    return nullptr;
}

bool CClassInfo::HasField( CSymbol s )
{
    if( fields.find( s ) != fields.end() ) {
        return true;
    }

    if( parentInfo != nullptr ) {
        return parentInfo->HasField( s );
    }

    return false;
}

CMethodInfo* CClassInfo::GetMethod( CSymbol s )
{
    if( methods.find( s ) != methods.end() ) {
        return methods[s].get();
    }

    if( parentInfo != nullptr ) {
        return parentInfo->GetMethod( s );
    }

    return nullptr;
}

int CClassInfo::GetSize() const
{
    return static_cast<int>(fields.size()) + ( parent == nullptr ? 0 : parentInfo->GetSize() );
}

bool CClassInfo::HasMethod( CSymbol s )
{
    if( methods.find( s ) != methods.end() ) {
        return true;
    }

    if( parentInfo != nullptr ) {
        return parentInfo->HasMethod( s );
    }

    return false;
}

const CSymbol& CClassInfo::GetName() const
{
    return name;
}

const CSymbol& CClassInfo::GetParent() const
{
    return parent;
}

const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& CClassInfo::GetFields() const
{
    return fields;
}

void CClassInfo::SetMethod( const CSymbol& methodName, std::unique_ptr<CMethodInfo>&& method )
{
    methods[methodName] = std::move( method );
}

const CClassInfo* CClassInfo::GetParentInfo() const
{
    return parentInfo;
}

void CClassInfo::SetParent( const CSymbol& parent_ )
{
    parent = parent_;
}

void CClassInfo::SetParentInfo( CClassInfo* info )
{
    parentInfo = info;
}

void CClassInfo::SetField( const CSymbol& varName, std::unique_ptr<CVariableInfo>&& varInfo )
{
    fields[varName] = std::move( varInfo );
}

const CSymbol& CClassInfo::BeginMethodName()
{
    return methods.begin()->second->GetName();
}
