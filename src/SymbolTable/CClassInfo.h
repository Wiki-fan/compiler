#pragma once

#include "utils.h"
#include "CMethodInfo.h"


class CClassInfo
{

 public:
    explicit CClassInfo( CSymbol name_ ) : name( name_ )
    {
    }

    CVariableInfo* GetField( CSymbol s );
    bool HasField( CSymbol s );
    bool HasMethod( CSymbol s );
    CMethodInfo* GetMethod( CSymbol s );
    int GetSize() const;
    const CSymbol& GetName() const;
    const CSymbol& GetParent() const;
    const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& GetFields() const;
    const CClassInfo* GetParentInfo() const;
    void SetMethod( const CSymbol& methodName, std::unique_ptr<CMethodInfo>&& method );
    void SetParent( const CSymbol& parent_ );
    void SetParentInfo( CClassInfo* info );
    void SetField( const CSymbol& varName, std::unique_ptr<CVariableInfo>&& varInfo );
    const CSymbol& BeginMethodName();

 private:
    CSymbol name;
    CSymbol parent = CSymbol( nullptr );
    std::map<CSymbol, std::unique_ptr<CVariableInfo>> fields;
    std::map<CSymbol, std::unique_ptr<CMethodInfo>> methods;
    CClassInfo* parentInfo = nullptr;
};
