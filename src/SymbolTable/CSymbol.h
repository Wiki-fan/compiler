#pragma once

#include <string>


class CSymbol;

class CSymbolInternals
{

 public:
    explicit CSymbolInternals( const std::string& string_ ) :
        string( string_ ), num( counter++ )
    {
    }

    const std::string& String() const
    {
        return string;
    }

    CSymbolInternals() = delete;
    CSymbolInternals( const CSymbolInternals& ) = delete;
    void operator=( const CSymbolInternals& ) = delete;

    static void ResetCounter()
    {
        counter = 0;
    }

    friend class CSymbol;

 private:
    const std::string& string;
    int num;
    static int counter;
};

class CSymbol
{

 public:
    CSymbol() = delete;

    explicit CSymbol( const CSymbolInternals* symbolInternals ) :
        s( symbolInternals )
    {
    }

    explicit CSymbol( const std::nullptr_t& symbolInternals )
    {
        *this = None;
    }

    explicit CSymbol( std::nullptr_t&& symbolInternals )
    {
        *this = None;
    }

    bool operator<( const CSymbol& other ) const
    {
        return s->num < other.s->num;
    }

    bool operator==( const CSymbol& other ) const
    {
        if( s == nullptr || other.s == nullptr ) {
            return s == other.s;
        }
        return s->num == other.s->num; // s == other.s &&
    }

    bool operator==( const std::nullptr_t& other ) const
    {
        return operator==( None );
    }

    bool operator!=( const CSymbol& other ) const
    {
        return !operator==( other );
    }

    bool operator!=( const std::nullptr_t& other ) const
    {
        return !operator==( other );
    }

    CSymbol& operator=( const CSymbol& other ) = default;

    CSymbol& operator=( const std::nullptr_t& other )
    {
        *this = None;
        return *this;
    }

    const CSymbolInternals& operator*() const
    {
        return *s;
    }

    const CSymbolInternals* operator->() const
    {
        return s;
    }

    // TODO: what if trying to compare symbols from different tables?

    static CSymbol None;

 private:
    const CSymbolInternals* s;
};
