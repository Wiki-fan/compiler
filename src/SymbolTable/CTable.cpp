#include "CTable.h"
#include <src/AST/nodes/CType.h>


CType CTable::typeVoid = CType( CType::TYPE::VOID );
CType CTable::typeStringArr = CType( CType::TYPE::STR_ARR );
