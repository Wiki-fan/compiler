#include "CTypeChecker.h"
#include <cassert>


bool CTypeChecker::hasClass( CSymbol className )
{
    return table->classes.find( className ) != table->classes.end();
}

bool CTypeChecker::hasVariable( CSymbol varName )
{
    const CVariableInfo* var = findVariableInfo( varName );
    return var != nullptr;
}

bool CTypeChecker::notFoundAdded( CSymbol className )
{
    return notFoundSymbol.insert( className ).second;
}

template<>
bool CTypeChecker::checkSymbol<CTypeChecker::ID_TYPE::CLASS>( CSymbol name, int line )
{
    if( !hasClass( name ) ) {
        if( notFoundAdded( name ) ) {
            std::string error = string_format(
                "Class \"%s\" doesn't exist (line %d)",
                name->String().c_str(),
                line );
            errors.push_back( error );
        }
        return true;
    }
    return false;
}

template<>
bool CTypeChecker::checkSymbol<CTypeChecker::ID_TYPE::METHOD>( CSymbol name, int line, CSymbol callerTypeName )
{
    CClassInfo* callerClass = table->classes[callerTypeName].get();
    if( !callerClass->HasMethod( name ) ) {
        if( notFoundAdded( name ) ) {
            std::string error = string_format(
                "Class \"%s\" has no method \"%s\" (line %d)",
                callerClass->GetName()->String().c_str(),
                name->String().c_str(),
                line );
            errors.push_back( error );
        }
        return true;
    }
    return false;
}

template<>
bool CTypeChecker::checkSymbol<CTypeChecker::ID_TYPE::VARIABLE>( CSymbol name, int line )
{
    if( !hasVariable( name ) ) {
        if( notFoundAdded( name ) ) {
            std::string error = string_format(
                "Variable \"%s\" doesn't exist (line %d)",
                name->String().c_str(),
                line );
            errors.push_back( error );
        }
        return true;
    }
    return false;
}

template<>
bool CTypeChecker::checkSymbol<CTypeChecker::ID_TYPE::TYPE>( CSymbol name, int line )
{
    if( !hasClass( name ) ) {
        if( notFoundAdded( name ) ) {
            std::string error = string_format(
                "CType %s not found (line %d)",
                name->String().c_str(),
                line );
            errors.push_back( error );
        }
        return true;
    }
    return false;
}

bool CTypeChecker::cyclicInheritance( CSymbol childName )
{
    CSymbol parent = table->classes[childName]->GetParent();
    while( parent != CSymbol::None ) {
        if( childName == parent ) {
            return true;
        }
        if( !hasClass( parent ) ) {
            // Проверяемый предок не объявлен.
            // Ошибка на объявлении будет добавлено при Visit на потомке этого предка.
            return false;
        }
        parent = table->classes.find( parent )->second->GetParent();
    }
    return false;
}

const CVariableInfo* CTypeChecker::findVariableInfo( CSymbol s )
{
    if( curMethod != nullptr ) {
        if( curMethod->ArgExist( s ) ) {
            return curMethod->GetArg( s );
        }
        if( curMethod->LocalExist( s ) ) {
            return curMethod->GetLocal( s );
        }
    }
    return curClass->GetField( s );
}

void CTypeChecker::Visit( const CGoal* n )
{
    n->GetMainClass()->Accept( this );
    for( auto& classDeclaration : *n->GetClassDeclarations() ) {
        classDeclaration->Accept( this );
    }
}

void CTypeChecker::Visit( const CMainClass* n )
{
    curClass = table->classes[n->GetId()->GetValue()].get();
    curMethod = curClass->GetMethod( n->GetMainId()->GetValue() );
    n->GetStatement()->Accept( this );
    curClass = nullptr;
    curMethod = nullptr;
}

void CTypeChecker::Visit( const CClassDeclaration* n )
{
    // Existence check not needed
    curClass = table->classes[n->GetId()->GetValue()].get();

    if( curClass->GetParent() != CSymbol::None ) {
        checkSymbol<ID_TYPE::CLASS>( curClass->GetParent(), n->fstLine );
        curClass->SetParentInfo( table->classes[curClass->GetParent()].get() );
    }

    if( cyclicInheritance( curClass->GetName() ) ) {
        std::string error = string_format(
            "Cyclic inheritance found (line %d).",
            n->fstLine );
        errors.push_back( error );
    }
    for( auto& methodDeclaration: *n->GetMethodDeclarations() ) {
        methodDeclaration->Accept( this );
    }
    curClass = nullptr;
}

void CTypeChecker::Visit( const CVarDeclaration* n )
{
    // pass
}

void CTypeChecker::Visit( const CArgument* n )
{
    // pass
}

void CTypeChecker::Visit( const CMethodDeclaration* n )
{
    // Existence check not needed
    curMethod = curClass->GetMethod( n->GetId()->GetValue() );

    // Проверка существования CUSTOM типов аргументов
    for( auto& arg : *n->GetArguments() ) {
        if( arg->GetType()->GetType() == CType::TYPE::CUSTOM ) {
            checkSymbol<ID_TYPE::CLASS>( arg->GetType()->GetName()->GetValue(), n->fstLine );
        }
    }
    for( auto& statement : *n->GetStatements() ) {
        statement->Accept( this );
    }

    n->GetReturnExp()->Accept( this );
    CTypeInfo returnExpressionType = curType;
    if( returnExpressionType != CTypeInfo( *n->GetType() ) ) {
        std::string error =
            string_format( "The type of return value %s does not match the function return type %s. (line %d)",
                           returnExpressionType.GetString().c_str(), n->GetType()->GetString().c_str(), n->fstLine );
        errors.push_back( error );
    }
    curMethod = nullptr;
}

void CTypeChecker::Visit( const CType* n )
{
    assert( false );
}

void CTypeChecker::Visit( const CComplexStm* n )
{
    for( auto& statement : *n->GetStatements() ) {
        statement->Accept( this );
    }
}

void CTypeChecker::Visit( const CIfStm* n )
{
    n->GetExp()->Accept( this );
    CTypeInfo condType = curType;
    if( condType != CTypeInfo::Boolean ) {
        std::string error = string_format( "Expected condition of type boolean. Have %s instead (line %d)",
                                           condType.GetString().c_str(), n->fstLine );
        errors.push_back( error );
    }
    n->GetIfStm()->Accept( this );
    n->GetElseStm()->Accept( this );
}

void CTypeChecker::Visit( const CWhileStm* n )
{
    n->GetExp()->Accept( this );
    CTypeInfo condType = curType;
    if( condType != CTypeInfo::Boolean ) {
        std::string error = string_format( "Expected condition of type boolean. Have %s instead (line %d)",
                                           condType.GetString().c_str(),
                                           n->fstLine );
        errors.push_back( error );
    }
    n->GetStm()->Accept( this );
}

void CTypeChecker::Visit( const CPrintStm* n )
{
    n->GetExp()->Accept( this );
    if( curType != CTypeInfo::Int ) {
        std::string error = string_format( "Expected condition of type int. Have %s instead (line %d)",
                                           curType.GetString().c_str(),
                                           n->fstLine );
        errors.push_back( error );
    }
}

void CTypeChecker::Visit( const CAssignmentStm* n )
{
    n->GetId()->Accept( this );
    if( checkSymbol<ID_TYPE::VARIABLE>( n->GetId()->GetValue()->GetValue(), n->fstLine ) ) {
        return;
    }
    const CVariableInfo* lVar = findVariableInfo( n->GetId()->GetValue()->GetValue() );
    CTypeInfo lType = CTypeInfo( *lVar->GetType() );
    n->GetExp()->Accept( this );
    CTypeInfo rType = curType;
    if( lType != rType ) {
        std::string error = string_format(
            "Assigning expression of type %s to variable of type %s (line %d)",
            lType.GetString().c_str(),
            rType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
}

void CTypeChecker::Visit( const CArrayAssignmentStm* n )
{
    n->GetId()->Accept( this );
    if( checkSymbol<ID_TYPE::VARIABLE>( n->GetId()->GetValue()->GetValue(), n->fstLine ) ) {
        return;
    }
    const CVariableInfo* lVar = findVariableInfo( n->GetId()->GetValue()->GetValue() );
    CTypeInfo lType = CTypeInfo( *lVar->GetType() );
    n->GetIndex()->Accept( this );
    CTypeInfo indexType = curType;
    n->GetResult()->Accept( this );
    CTypeInfo rType = curType;

    if( lType != CTypeInfo::IntArr ) {
        std::string error = string_format(
            "Trying to get index of scalar variable %s (line %d)",
            n->GetId()->GetValue()->GetValue()->String().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    if( indexType != CTypeInfo::Int ) {
        std::string error = string_format(
            "Trying to index array using expression of type %s (not int) (line %d)",
            indexType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    if( rType != CTypeInfo::Int ) {
        std::string error = string_format(
            "Trying to assign %s to array element of type int (line %d)",
            rType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
}

void CTypeChecker::Visit( const CBinaryExp* n )
{
    n->GetLeft()->Accept( this );
    CTypeInfo op1Type = curType;
    n->GetRight()->Accept( this );
    CTypeInfo op2Type = curType;
    if( n->GetType() == CBinaryExp::TYPE::AND ) {
        if( op1Type != CTypeInfo::Boolean || op2Type != CTypeInfo::Boolean ) {
            std::string error = string_format(
                "Applying operation %s to values of wrong types: got %s and %s (line %d)",
                n->GetTypeStr().c_str(),
                op1Type.GetString().c_str(),
                op2Type.GetString().c_str(),
                n->fstLine );
            errors.push_back( error );
        }
        curType = CTypeInfo::Boolean;
    } else if( n->GetType() == CBinaryExp::TYPE::LESS ) {
        if( op1Type != CTypeInfo::Int || op2Type != CTypeInfo::Int ) {
            std::string error = string_format(
                "Applying operation %s to values of wrong types: got %s and %s (line %d)",
                n->GetTypeStr().c_str(),
                op1Type.GetString().c_str(),
                op2Type.GetString().c_str(),
                n->fstLine );
            errors.push_back( error );
        }
        curType = CTypeInfo::Boolean;
    } else {
        if( op1Type != CTypeInfo::Int || op2Type != CTypeInfo::Int ) {
            std::string error = string_format(
                "Applying operation %s to values of wrong types: got %s and %s (line %d)",
                n->GetTypeStr().c_str(),
                op1Type.GetString().c_str(),
                op2Type.GetString().c_str(),
                n->fstLine );
            errors.push_back( error );
        }
        curType = CTypeInfo::Int;
    }
}

void CTypeChecker::Visit( const CIndexExp* n )
{
    n->GetBase()->Accept( this );
    CTypeInfo arrType = curType;
    if( arrType != CTypeInfo::IntArr ) {
        std::string error = string_format(
            "Expected array of type int[]. Have %s instead (line %d)",
            curType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    n->GetIndex()->Accept( this );
    CTypeInfo indexType = curType;
    if( indexType != CTypeInfo::Int ) {
        std::string error = string_format(
            "Expected array index of type int. Have %s instead (line %d)",
            curType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    curType = CTypeInfo::Int;
}

void CTypeChecker::Visit( const CLengthExp* n )
{
    n->GetExp()->Accept( this );
    CTypeInfo arrType = curType;
    if( arrType != CTypeInfo::IntArr ) {
        std::string error = string_format(
            "Trying to get length of type %s (not int[]) (line %d)",
            curType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    curType = CTypeInfo::Int;
}

void CTypeChecker::Visit( const CMethodCallExp* n )
{
    n->GetCaller()->Accept( this );
    CTypeInfo callerType = curType;
    if( callerType.GetType() != CType::TYPE::CUSTOM ) {
        std::string error = string_format(
            "Calling function from expression of primitive type %s (line %d)",
            callerType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
        curType = CTypeInfo( CType::TYPE::UNKNOWN );
        return;
    }
    if( checkSymbol<ID_TYPE::CLASS>( callerType.GetName(), n->fstLine ) ) {
        curType = CTypeInfo( CType::TYPE::UNKNOWN );
        return;
    }

    if( checkSymbol<ID_TYPE::METHOD>( n->GetFuncName()->GetValue(), n->fstLine, callerType.GetName() ) ) {
        curType = CTypeInfo( CType::TYPE::UNKNOWN );
        return;
    }

    CClassInfo* callerClass = table->classes[callerType.GetName()].get();
    CMethodInfo* method = callerClass->GetMethod( n->GetFuncName()->GetValue() );
    if( method->GetArgs().size() != n->GetArgs()->size() ) {
        std::string error = string_format(
            "Function call \"%s\" takes %d arguments but %d was provided (line %d)",
            method->GetName()->String().c_str(),
            method->GetArgs().size(),
            n->GetArgs()->size(),
            n->fstLine );
        errors.push_back( error );
        curType = CTypeInfo( CType::TYPE::UNKNOWN );
        return;
    }

    for( auto& arg : *n->GetArgs() ) {
        arg->Accept( this );
    }

    curType = CTypeInfo( *method->GetRetType() );
}

void CTypeChecker::Visit( const CIntegerExp* n )
{
    curType = CTypeInfo::Int;
}

void CTypeChecker::Visit( const CBooleanConstExp* n )
{
    curType = CTypeInfo::Boolean;
}

void CTypeChecker::Visit( const CIdentifierExp* n )
{
    //n->value->Accept( this );
    if( checkSymbol<ID_TYPE::VARIABLE>( n->GetValue()->GetValue(), n->fstLine ) ) {
        curType = CTypeInfo( CType::TYPE::UNKNOWN );
    } else {
        const CVariableInfo* vi = findVariableInfo( n->GetValue()->GetValue() );
        curType = CTypeInfo( *vi->GetType() );
    }
}

void CTypeChecker::Visit( const CThisExp* n )
{
    curType = CTypeInfo( CType::TYPE::CUSTOM, curClass->GetName() );
}

void CTypeChecker::Visit( const CNewArrExp* n )
{
    n->GetLength()->Accept( this );
    if( curType != CTypeInfo::Int ) {
        std::string error = string_format(
            "Expected array index of type int. Have %s instead (line %d)",
            curType.GetString().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    curType = CTypeInfo::IntArr;
}

void CTypeChecker::Visit( const CNewExp* n )
{
    checkSymbol<ID_TYPE::TYPE>( n->GetType()->GetValue(), n->fstLine );
    curType = CTypeInfo( CType::TYPE::CUSTOM, n->GetType()->GetValue() );
}

void CTypeChecker::Visit( const CNegationExp* n )
{
    n->GetOp()->Accept( this );
    if( curType != CTypeInfo::Boolean ) {
        std::string error =
            string_format( "Operand of CNegationExp is %s instead of boolean (line %d)",
                           curType.GetString().c_str(),
                           n->fstLine );
        errors.push_back( error );
    }
    // keep curType
}

void CTypeChecker::Visit( const CParenthesesExp* n )
{
    n->GetExp()->Accept( this );
    // keep curType
}

void CTypeChecker::Visit( const CIdentifier* n )
{
    //assert(false);
}

void CTypeChecker::printErrors()
{
    for( int i = 0; i < errors.size(); ++i ) {
        printf( "ERROR %d: %s\n", i, errors[i].c_str() );
    }
    if( !errors.empty() ) {
        throw CompilerException();
    }
}
