#pragma once

#include <stack>
#include <set>
#include <src/common.h>
#include "src/AST/visitors/IVisitor.h"
#include "CTable.h"
#include "CTypeInfo.h"


class CTypeChecker : public IVisitor
{

 public:
    explicit CTypeChecker( CTable* table ) :
        table( table ),
        curClass( nullptr ),
        curMethod( nullptr ),
        curType( CType::TYPE::CUSTOM )
    {
    }

    void printErrors();

    const CVariableInfo* findVariableInfo( CSymbol s );

    void Visit( const CGoal* n ) override;
    void Visit( const CMainClass* n ) override;
    void Visit( const CClassDeclaration* n ) override;
    void Visit( const CVarDeclaration* n ) override;
    void Visit( const CArgument* n ) override;
    void Visit( const CMethodDeclaration* n ) override;

    void Visit( const CType* n ) override;

    void Visit( const CIfStm* n ) override;
    void Visit( const CWhileStm* n ) override;
    void Visit( const CComplexStm* n ) override;
    void Visit( const CPrintStm* n ) override;
    void Visit( const CAssignmentStm* n ) override;
    void Visit( const CArrayAssignmentStm* n ) override;

    void Visit( const CBinaryExp* n ) override;
    void Visit( const CIndexExp* n ) override;
    void Visit( const CLengthExp* n ) override;
    void Visit( const CMethodCallExp* n ) override;
    void Visit( const CIntegerExp* n ) override;
    void Visit( const CBooleanConstExp* n ) override;
    void Visit( const CIdentifierExp* n ) override;
    void Visit( const CThisExp* n ) override;
    void Visit( const CNewArrExp* n ) override;
    void Visit( const CNewExp* n ) override;
    void Visit( const CNegationExp* n ) override;
    void Visit( const CParenthesesExp* n ) override;

    void Visit( const CIdentifier* n ) override;

 private:
    enum class ID_TYPE
    {
        CLASS, METHOD, VARIABLE, TYPE
    };

    CTable* table;
    CClassInfo* curClass;
    CMethodInfo* curMethod;
    std::vector<std::string> errors;
    // Тип, прокидываемый наверх при проверки ITreeNode
    CTypeInfo curType;
    // Мн-во необъявленных символов
    std::set<CSymbol> notFoundSymbol;

    // Возвращает true, если на child образуется циклическое наследование.
    bool cyclicInheritance( CSymbol childName );
    // Возвращает true,если className есть в таблице
    bool hasClass( CSymbol className );
    // Возвращает true,если varName есть в таблице
    bool hasVariable( CSymbol varName );

    // Добавляет errorMsg, если необъявленный символ уже есть в notFoundIdentifiers
    // Возвращает true, если символ необъявле
    template<ID_TYPE type, typename... Args>
    bool checkSymbol( CSymbol name, int line, Args... );
    //  Возвращает true, если ненайденный символ уже был добавлен
    bool notFoundAdded( CSymbol className );
};
