#include "CTypeInfo.h"


CTypeInfo CTypeInfo::Int( CType::TYPE::INT );
CTypeInfo CTypeInfo::Boolean( CType::TYPE::BOOLEAN );
CTypeInfo CTypeInfo::IntArr( CType::TYPE::INT_ARR );

CTypeInfo::CTypeInfo( const CType& t ) : type( t.GetType() ), name( nullptr )
{
    if( t.GetName() == nullptr ) {
        name = nullptr;
    } else {
        name = t.GetName()->GetValue();
    }
}

std::string CTypeInfo::GetString()
{
    switch( type ) {
        case CType::TYPE::INT_ARR:
            return "int[]";
        case CType::TYPE::INT:
            return "int";
        case CType::TYPE::BOOLEAN:
            return "boolean";
        case CType::TYPE::CUSTOM:
            return name->String();
        case CType::TYPE::UNKNOWN:
            return "UNKNOWN";
        case CType::TYPE::STR_ARR:
            return "String[]";
        case CType::TYPE::VOID:
            return "void";
        default:
            assert( false );
    }
}

CType::TYPE CTypeInfo::GetType() const
{
    return type;
}

const CSymbol& CTypeInfo::GetName() const
{
    return name;
}

bool CTypeInfo::operator==( const CTypeInfo& other ) const
{
    if( type == CType::TYPE::UNKNOWN || other.type == CType::TYPE::UNKNOWN ) {
        return true;
    }
    if( type != CType::TYPE::CUSTOM ) {
        return type == other.type;
    } else { return type == other.type && name == other.name; }
}

bool CTypeInfo::operator!=( const CTypeInfo& other ) const
{
    return !operator==( other );
}
