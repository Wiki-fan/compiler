#include <utility>


#pragma once

#include <exception>
#include <memory>
#include <regex>
#include <experimental/filesystem>


namespace fs = std::experimental::filesystem;

class CompilerException : public std::exception
{

 public:
    CompilerException() = default;

    explicit CompilerException( const std::string&& description_ ) : description( std::move( description_ ) )
    {
    }

    const char* what() const noexcept override
    {
        return description.c_str();
    }

 private:
    std::string description;
};

// From StackOverflow
template<typename ... Args>
std::string string_format( const std::string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf( new char[size] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

std::string GetPureFileName( const fs::path& path );
