#pragma once

#include <experimental/filesystem>
#include <boost/program_options.hpp>

#include "common.h"
#include <src/AST/nodes/CGoal.h>
#include "src/SymbolTable/CTable.h"
#include "src/SymbolTable/CStringPool.h"


enum class TCompilePhase
{
    PHASE_LEX,
    PHASE_PARSE,
    PHASE_AST,
    PHASE_SYMBOLTABLE,
    PHASE_IRT,
    PHASE_ALL
};

typedef boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::program_options::multiple_occurrences>>
    duplicationException;

namespace fs = std::experimental::filesystem;

class Compiler
{
 
 public:
    Compiler( TCompilePhase phase, FILE* input, FILE* output, bool debug, const std::string& name = "program",
              const fs::path& outputFilePath = fs::path( "" ) );

    void Compile();
    static Compiler CompilerFromArgs( int argc, char* argv[] );
    void AddErrorMessage( const std::string& errMsg );
    void PrintErrors() const;
    std::string GetPureFileName();
    int GetBegin() const;
    CStringPool& GetStringPool();
    FILE* GetOutput();
    TCompilePhase GetPhase() const;
    void ShiftBegin( int shift );
    void SetAst( CGoal* goal );

 private:
    std::vector<std::string> errors;
    int begin = 0;
    std::unique_ptr<CGoal> ast;
    CStringPool stringPool;
    FILE* input;
    FILE* output;
    fs::path inputFilePath;
    fs::path outputPath;
    TCompilePhase phase;
    bool debug;
    std::unique_ptr<CTable> symbolTable;

    Compiler();
    // Инициализирует input, output согласно флагам.
    void initIORoutines( const boost::program_options::variables_map& vm );
    bool errorsOccurred() const;
};
