%debug
%define parse.error verbose
//%define parse.lac full
%define api.pure full
//%pure-parser
%locations
%token-table

%code requires {
#include "src/compiler.h"
#include "src/common.h"
typedef void* yyscan_t;

#include <iostream>
#include "src/AST/nodes/CArgument.h"
#include "src/AST/nodes/CClassDeclaration.h"
#include "src/AST/nodes/CExp.h"
#include "src/AST/nodes/ITreeNodes.h"
#include "src/AST/nodes/CMethodDeclaration.h"
#include "src/AST/nodes/CStm.h"
#include "src/AST/nodes/CType.h"
#include "src/AST/nodes/CVarDeclaration.h"
using namespace AST;
}

%code {
	#include <lexer.h>

	void yyerror(YYLTYPE* yylloc, yyscan_t scanner, Compiler* compiler, const char *msg)
	{
		int yylineno = yyget_lineno(scanner);
		std::string errMsg = string_format("[Line %3d, column %3d]: %s",
											yylloc->first_line,
											yylloc->first_column,
											msg);
		compiler->AddErrorMessage(errMsg);
	}


	#define IF_AST(stmt) \
		if (compiler->GetPhase() >= TCompilePhase::PHASE_AST) { stmt }

	class ITreeNodeFactory
    {
     public:
        template<typename NodeType, typename... Args>
        static NodeType* Get( const YYLTYPE& loc, Args... args )
        {
            NodeType* node = new NodeType( args... );
            node->fstCol = loc.first_column;
            node->fstLine = loc.first_line;
            node->lastCol = loc.last_column;
            node->lastLine = loc.last_line;
            return node;
        }
    };
}

%param { yyscan_t scanner }
%parse-param { Compiler* compiler }

%union {
	int num;
	char* str;
	std::vector<std::unique_ptr<CArgument>>* arguments;
	std::vector<std::unique_ptr<CClassDeclaration>>* classDeclarations;
   	CClassDeclaration* classDeclaration;
   	IExp* expression;
   	std::vector<std::unique_ptr<IExp>>* argumentsExp;
   	CGoal* goal;
	CIdentifier* id;
	CMainClass* mainClass;
	std::vector<std::unique_ptr<CMethodDeclaration>>* methodDeclarations;
	CMethodDeclaration* methodDeclaration;
	IStm* statement;
	std::vector<std::unique_ptr<IStm>>* statements;
	std::vector<std::unique_ptr<CVarDeclaration>>* varDeclarations;
	CVarDeclaration* varDeclaration;
	CType* type;
}

%token <str> ID
%token <num> NUM

%token DOT
%token COMMA
%token SEMI

%token LBRACE
%token RBRACE
%token LBRACKET
%token RBRACKET
%token LPARENTH
%token RPARENTH

%token LOGICAL_TRUE
%token LOGICAL_FALSE
%token THIS
%token NEW
%token INT
%token IF
%token ELSE
%token WHILE
%token PRINT
%token CLASS
%token PUBLIC
%token STATIC
%token VOID
%token MAIN
%token STRING
%token EXTENDS
%token PRIVATE
%token LENGTH
%token BOOLEAN
%token RETURN
%token PLUS MINUS MUL DIV EQ
%token LOGICAL_AND LESS EXCL_MARK
%token EOF_TOKEN 0
%token ERROR_TOKEN


%printer { fprintf(stderr, "%s", $$); } ID
%printer { fprintf(stderr, "%d", $$); } NUM


%left PLUS MINUS MUL LESS LOGICAL_AND
%precedence EXCL_MARK
%precedence DOT
%precedence LBRACKET


%type <arguments> Arguments
%type <classDeclarations> ClassDeclarations
%type <classDeclaration> ClassDeclaration
%type <expression> Exp
%type <argumentsExp> ArgumentsExp
%type <goal> CGoal
%type <id> Identifier
%type <mainClass> CMainClass
%type <methodDeclarations> MethodDeclarations
%type <methodDeclaration> CMethodDeclaration
%type <statement> Stm
%type <statements> Stms
%type <type> Type
%type <varDeclarations> VarDeclarations
%type <varDeclaration> CVarDeclaration


%destructor { } <num>
%destructor { delete $$; } <goal>
%destructor { free ($$); } <str>
%destructor { delete $$; } <*>

%%

CGoal:
  	CMainClass ClassDeclarations EOF_TOKEN {
  		@$.first_column = @1.first_column;
        @$.first_line = @1.first_line;
        @$.last_column = @3.last_column;
        @$.last_line = @3.last_line;
  		compiler->SetAst(ITreeNodeFactory::Get<CGoal>(@$, $1, $2));
		$$ = nullptr;
  	}
  	| error CMainClass ClassDeclarations EOF_TOKEN { yyerrok; delete $2; delete $3; $$ = nullptr; }
  	| CMainClass ClassDeclarations error EOF_TOKEN { yyerrok; delete $1; delete $2; $$ = nullptr; }
;

CMainClass:
  	CLASS Identifier LBRACE PUBLIC STATIC VOID MAIN LPARENTH STRING LBRACKET RBRACKET Identifier RPARENTH
    LBRACE Stm RBRACE RBRACE {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @17.last_column;
		@$.last_line = @17.last_line;
		auto mainId = ITreeNodeFactory::Get<CIdentifier>(@7, compiler->GetStringPool().GetIntern(std::string("main")));
    	$$ = ITreeNodeFactory::Get<CMainClass>(@$, $2, $12, $15, mainId);
    }
;

ClassDeclarations:
    %empty {
    	$$ = new std::vector<std::unique_ptr<CClassDeclaration>>();
    }
    | ClassDeclarations ClassDeclaration {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @2.last_column;
		@$.last_line = @2.last_line;
    	$1->push_back(std::unique_ptr<CClassDeclaration>($2));
    	$$ = $1;
    }
    | ClassDeclarations error ClassDeclaration { yyerrok; delete $1; delete $3; $$ = nullptr; }
;

ClassDeclaration:
    CLASS Identifier LBRACE VarDeclarations MethodDeclarations RBRACE {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @6.last_column;
		@$.last_line = @6.last_line;
    	$$ = ITreeNodeFactory::Get<CClassDeclaration>(@$, $2, nullptr, $4, $5);
   	}
    | CLASS Identifier EXTENDS Identifier LBRACE VarDeclarations MethodDeclarations RBRACE {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @8.last_column;
		@$.last_line = @8.last_line;
 	    $$ = ITreeNodeFactory::Get<CClassDeclaration>(@$, $2, $4, $6, $7);
    }
    | CLASS error LBRACE VarDeclarations MethodDeclarations RBRACE { yyerrok; delete $4; delete $5; $$ = nullptr; }
    | CLASS Identifier LBRACE error RBRACE { yyerrok; delete $2; $$ = nullptr; }
    | CLASS Identifier EXTENDS Identifier LBRACE error RBRACE { yyerrok; delete $2; delete $4; $$ = nullptr; }
;

VarDeclarations:
	%empty {
		$$ = new std::vector<std::unique_ptr<CVarDeclaration>>();
	}
    | VarDeclarations CVarDeclaration {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @2.last_column;
		@$.last_line = @2.last_line;
    	$1->push_back(std::unique_ptr<CVarDeclaration>($2));
    	$$ = $1;
    }
;

CVarDeclaration:
  	Type Identifier SEMI {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @3.last_column;
		@$.last_line = @3.last_line;
  	  	$$ = ITreeNodeFactory::Get<CVarDeclaration>(@$, $1, $2);
  	 }
  	| Type error SEMI { yyerrok; delete $1; $$ = nullptr; }
  	//| Type error RBRACE { yyerrok; $$ = nullptr; } TODO: another missing semicolon error. It can be fix if put back RBRACE
;

MethodDeclarations:
	%empty {
		$$ = new std::vector<std::unique_ptr<CMethodDeclaration>>();
	}
    | MethodDeclarations CMethodDeclaration {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @2.last_column;
		@$.last_line = @2.last_line;
  		$1->push_back(std::unique_ptr<CMethodDeclaration>($2));
    	$$ = $1;
    }
;

CMethodDeclaration:
    PUBLIC Type Identifier LPARENTH Arguments RPARENTH LBRACE VarDeclarations Stms RETURN Exp SEMI RBRACE {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @13.last_column;
		@$.last_line = @13.last_line;
   	    $$ = ITreeNodeFactory::Get<CMethodDeclaration>(@$, $2, $3, $5, $8, $9, $11);
    }
    | PUBLIC Type Identifier LPARENTH error RPARENTH
    	LBRACE VarDeclarations Stms RETURN Exp SEMI RBRACE { yyerrok; delete $2; delete $3; delete $8; delete $9; delete $11; $$ = nullptr; }
	| PRIVATE Type Identifier LPARENTH Arguments RPARENTH LBRACE VarDeclarations Stms RETURN Exp SEMI RBRACE {
	    yyerror(&@1, scanner, compiler, "private modifier is not allowed");
	    @$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @13.last_column;
		@$.last_line = @13.last_line;
		$$ = ITreeNodeFactory::Get<CMethodDeclaration>(@$, $2, $3, $5, $8, $9, $11);}
;

Arguments:
    %empty {
    	$$ = new std::vector<std::unique_ptr<CArgument>>();
    }
    | Type Identifier {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @2.last_column;
		@$.last_line = @2.last_line;
   	    $$ = new std::vector<std::unique_ptr<CArgument>>();
     	$$->push_back(std::unique_ptr<CArgument>(ITreeNodeFactory::Get<CArgument>(@$, $1, $2)));
    }
    | Arguments COMMA Type Identifier {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @4.last_column;
		@$.last_line = @4.last_line;
    	$1->push_back(std::unique_ptr<CArgument>(ITreeNodeFactory::Get<CArgument>(@$, $3, $4)));
    	$$ = $1;
    }
    //| Arguments COMMA error { yyerrok; $$ = nullptr; }
;

Type:
  	INT LBRACKET RBRACKET {
  		@$.first_column = @1.first_column;
  		@$.first_line = @1.first_line;
  		@$.last_column = @3.last_column;
  		@$.last_line = @3.last_line;
  		$$ = ITreeNodeFactory::Get<CType>(@$, CType::TYPE::INT_ARR);
  	}
    | BOOLEAN {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
    	$$ = ITreeNodeFactory::Get<CType>(@$, CType::TYPE::BOOLEAN);
   	}
    | INT {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
    	$$ = ITreeNodeFactory::Get<CType>(@$, CType::TYPE::INT);
	}
    | Identifier {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
     	$$ = ITreeNodeFactory::Get<CType>(@$, CType::TYPE::CUSTOM, $1);
    }
;

Stms:
    %empty {
    	$$ = new std::vector<std::unique_ptr<IStm>>();
    }
    | Stm Stms {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
   		@$.last_column = @2.last_column;
   		@$.last_line = @2.last_line;
    	$2->insert($2->begin(), std::unique_ptr<IStm>($1));
    	$$ = $2;
    }
;

Stm:
    LBRACE Stms RBRACE {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
   		@$.last_column = @3.last_column;
   		@$.last_line = @3.last_line;
       	$$ = ITreeNodeFactory::Get<CComplexStm>(@$, $2);
    }
    | IF LPARENTH Exp RPARENTH Stm ELSE Stm {
   		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
   		@$.last_column = @7.last_column;
   		@$.last_line = @7.last_line;
        $$ = ITreeNodeFactory::Get<CIfStm>(@$, $3, $5, $7);
    }
    | WHILE LPARENTH Exp RPARENTH Stm {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @5.last_column;
		@$.last_line = @5.last_line;
    	$$ = ITreeNodeFactory::Get<CWhileStm>(@$, $3, $5);
   	}
    | PRINT LPARENTH Exp RPARENTH SEMI {
   		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @5.last_column;
		@$.last_line = @5.last_line;
		$$ = ITreeNodeFactory::Get<CPrintStm>(@$, $3);
    }
    | Identifier EQ Exp SEMI {
       		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @4.last_column;
       		@$.last_line = @4.last_line;
    		$$ = ITreeNodeFactory::Get<CAssignmentStm>(@$, ITreeNodeFactory::Get<CIdentifierExp>(@1, $1), $3);
    }
	| Identifier LBRACKET Exp RBRACKET EQ Exp SEMI {
		@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
		@$.last_column = @7.last_column;
		@$.last_line = @7.last_line;
		$$ = ITreeNodeFactory::Get<CArrayAssignmentStm>(@$, ITreeNodeFactory::Get<CIdentifierExp>(@1, $1), $3, $6);
	}
    | LBRACE error RBRACE { yyerrok; $$ = nullptr; }
    | IF LPARENTH error RPARENTH Stm ELSE Stm { yyerrok; delete $5; delete $7; $$ = nullptr; }
    | WHILE LPARENTH error RPARENTH Stm { yyerrok; delete $5; $$ = nullptr; }
    | error SEMI { yyerrok; $$ = nullptr; }

;

ArgumentsExp:
    %empty {
    	$$ = new std::vector<std::unique_ptr<IExp>>();
    }
    | Exp {
    	@$.first_column = @1.first_column;
		@$.first_line = @1.first_line;
    	$$ = new std::vector<std::unique_ptr<IExp>>();
		$$->push_back(std::unique_ptr<IExp>($1));
    }
    | ArgumentsExp COMMA Exp {
  		@$.first_column = @1.first_column;
   		@$.first_line = @1.first_line;
   		@$.last_column = @3.last_column;
   		@$.last_line = @3.last_line;
    	$1->push_back(std::unique_ptr<IExp>($3));
    	$$ = $1;
    }
    //| ArgumentsExp COMMA error { yyerrok; $$ = nullptr; }
;

Exp:
    Exp LOGICAL_AND Exp {
   			@$.first_column = @1.first_column;
    		@$.first_line = @1.first_line;
    		@$.last_column = @3.last_column;
    		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CBinaryExp>(@$, CBinaryExp::TYPE::AND, $1, $3);
    }
    | Exp LESS Exp {
   			@$.first_column = @1.first_column;
    		@$.first_line = @1.first_line;
    		@$.last_column = @3.last_column;
    		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CBinaryExp>(@$, CBinaryExp::TYPE::LESS, $1, $3);
    }
    | Exp PLUS Exp {
   			@$.first_column = @1.first_column;
    		@$.first_line = @1.first_line;
    		@$.last_column = @3.last_column;
    		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CBinaryExp>(@$, CBinaryExp::TYPE::PLUS, $1, $3);
   	}
    | Exp MINUS Exp {
   			@$.first_column = @1.first_column;
    		@$.first_line = @1.first_line;
    		@$.last_column = @3.last_column;
    		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CBinaryExp>(@$, CBinaryExp::TYPE::MINUS, $1, $3);
    }
    | Exp MUL Exp {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @3.last_column;
       		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CBinaryExp>(@$, CBinaryExp::TYPE::MUL, $1, $3);
    }
    | Exp LBRACKET Exp RBRACKET {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @4.last_column;
       		@$.last_line = @4.last_line;
    		$$ = ITreeNodeFactory::Get<CIndexExp>(@$, $1, $3);
    }
    | Exp LBRACKET error RBRACKET { yyerrok; delete $1; $$ = nullptr; }
    | Exp DOT LENGTH {
     		@$.first_column = @1.first_column;
      		@$.first_line = @1.first_line;
      		@$.last_column = @3.last_column;
      		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CLengthExp>(@$, $1);
    }
    | Exp DOT Identifier LPARENTH ArgumentsExp RPARENTH {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @6.last_column;
       		@$.last_line = @6.last_line;
    		$$ = ITreeNodeFactory::Get<CMethodCallExp>(@$, $1, $3, $5);
    }
    | NUM {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
    		$$ = ITreeNodeFactory::Get<CIntegerExp>(@$, $1);
    }
    | LOGICAL_TRUE {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
    		$$ = ITreeNodeFactory::Get<CBooleanConstExp>(@$, true);
    }
    | LOGICAL_FALSE {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
    		$$ = ITreeNodeFactory::Get<CBooleanConstExp>(@$, false);
     }
    | Identifier {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
    		$$ = ITreeNodeFactory::Get<CIdentifierExp>(@$, $1);
    }
    | THIS {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
    		$$ = ITreeNodeFactory::Get<CThisExp>(@$);
     }
    | NEW INT LBRACKET Exp RBRACKET {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @5.last_column;
       		@$.last_line = @5.last_line;
    		$$ = ITreeNodeFactory::Get<CNewArrExp>(@$, $4);
    }
    | NEW INT LBRACKET error RBRACKET { yyerrok; $$ = nullptr; }
    | NEW Identifier LPARENTH RPARENTH {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @4.last_column;
       		@$.last_line = @4.last_line;
    		$$ = ITreeNodeFactory::Get<CNewExp>(@$, $2);
    }
    | EXCL_MARK Exp {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @2.last_column;
       		@$.last_line = @2.last_line;
    		$$ = ITreeNodeFactory::Get<CNegationExp>(@$, $2);
     }
    | LPARENTH Exp RPARENTH {
      		@$.first_column = @1.first_column;
       		@$.first_line = @1.first_line;
       		@$.last_column = @3.last_column;
       		@$.last_line = @3.last_line;
    		$$ = ITreeNodeFactory::Get<CParenthesesExp>(@$, $2);
    }
    | LPARENTH error RPARENTH { yyerrok; $$ = nullptr; }
;

Identifier:
	ID {
			@$.first_column = @1.first_column;
			@$.first_line = @1.first_line;
			$$ = ITreeNodeFactory::Get<CIdentifier>(@$,
										  compiler->GetStringPool().GetIntern(std::string($1)));
			free($1);
	}
;

%%
