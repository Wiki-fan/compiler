#include "LineChecker.h"


long countMatches( const std::string& s, const std::regex& re )
{
    auto words_begin = std::sregex_iterator(
        s.begin(), s.end(), re );
    auto words_end = std::sregex_iterator();

    return std::distance( words_begin, words_end );
}

std::vector<int> get_true_error_lines( const std::string& filename )
{
    std::ifstream t( filename );
    std::string line;
    std::vector<int> error_lines;
    for( int linenum = 0; std::getline( t, line ); ++linenum ) {
        long count = countMatches( line, std::regex( "// HERE" ) );
        for( int i = 0; i < count; ++i ) {
            error_lines.push_back( linenum + 1 );
        }
    }
    return error_lines;
}

std::vector<int> get_result_error_lines( const std::string& re_string, const std::string& output )
{
    std::istringstream t( output );
    std::string line;
    std::vector<int> error_lines;
    std::regex re( re_string );
    for( int linenum = 0; std::getline( t, line ); ++linenum ) {
        std::smatch sm;
        if( std::regex_search( line, sm, re ) ) {
            int lineNumber = std::stoi( sm[1] );
            error_lines.push_back( lineNumber );
        }
    }
    return error_lines;
}
