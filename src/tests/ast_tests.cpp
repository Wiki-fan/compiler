#include <fstream>
#include <regex>
#include "gtest/gtest.h"
#include "../compiler.h"
#include "../common.h"
#include "string_reformers.h"


class TestAstWorks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestAstWorks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java",
     "BadSamples/TC_1a.java", "BadSamples/TC_2a.java", "BadSamples/TC_3a.java", "BadSamples/TC_3b.java",
     "BadSamples/TC_4a.java", "BadSamples/TC_4b.java", "BadSamples/TC_5a.java", "BadSamples/TC_6a.java",
     "BadSamples/TC_7b.java", "BadSamples/TC_7c.java", "BadSamples/TC_8a.java", "BadSamples/TC_11a.java",
     "BadSamples/TC_12b.java", "BadSamples/TC_13a.java", "BadSamples/TC_13b.java", "BadSamples/TC_13c.java",
     "BadSamples/TC_bonus1.java"
    };

class TestAstFails : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string> TestAstFails::files =
    {
        "BadSamples/LC_1.java",
        "BadSamples/TC_9a.java",
        "BadSamples/TC_9a_2.java",
        "BadSamples/TC_13d(optional).java"
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestAstWorks,
                         ::testing::ValuesIn( TestAstWorks::files ) );

TEST_P( TestAstWorks, ParsesSamples )
{
    std::string filename = GetParam();
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : ParsesSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_AST, input, stdout, true );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        throw;
    }
    std::string output = testing::internal::GetCapturedStdout();

    std::string correct = get_oneline( filename );

    fclose( input );

    ASSERT_EQ( correct, trim( output ) );
}
