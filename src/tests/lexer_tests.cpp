#include <fstream>
#include <regex>
#include "gtest/gtest.h"
#include "../compiler.h"
#include "../common.h"


std::set<std::string> KEYWORDS =
    {".", ",", ";", "{", "}", "[", "]", "(", ")", "+", "-", "*", "/", "=", "&&", "<", "!", "true", "false", "this",
     "new", "if", "else", "while", "System.out.println", "class", "public", "static", "void", "main", "String", "int",
     "boolean", "extends", "private", "return"};

void CheckIfIdIsKeyword( std::string s, const std::string& token )
{
    std::regex e( token + "\\(([^)]*)\\)" );
    std::smatch m;
    while( std::regex_search( s, m, e ) ) {
        std::string name = m[m.size() - 1];
        if( KEYWORDS.find( name ) != KEYWORDS.end() ) {
            throw CompilerException();
        }
        s = m.suffix().str();
    }
}

class TestLexerWorks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestLexerWorks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java",
     "BadSamples/TC_1a.java",
     "BadSamples/TC_2a.java",
     "BadSamples/TC_3a.java",
     "BadSamples/TC_3b.java",
     "BadSamples/TC_4a.java",
     "BadSamples/TC_4b.java",
     "BadSamples/TC_5a.java",
     "BadSamples/TC_6a.java",
     "BadSamples/TC_7b.java",
     "BadSamples/TC_7c.java",
     "BadSamples/TC_8a.java",
     "BadSamples/TC_9a.java",
     "BadSamples/TC_9a_2.java",
     "BadSamples/TC_11a.java",
     "BadSamples/TC_12b.java",
     "BadSamples/TC_13a.java",
     "BadSamples/TC_13b.java",
     "BadSamples/TC_13c.java",
     "BadSamples/TC_13d(optional).java",
     "BadSamples/TC_bonus1.java"};

class TestLexerFails : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string> TestLexerFails::files =
    {
        "BadSamples/LC_1.java",
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestLexerWorks,
                         ::testing::ValuesIn( TestLexerWorks::files ) );

TEST_P( TestLexerWorks, ParsesSamples )
{
    std::string filename = GetParam();
    std::cout << "Name:" << filename << std::endl;
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : ParsesSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_LEX, input, stdout, false );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        testing::internal::GetCapturedStdout();
        throw;
    }
    std::string output = testing::internal::GetCapturedStdout();
    CheckIfIdIsKeyword( output, "ID" );
    CheckIfIdIsKeyword( output, "NUM" );
    fclose( input );
}

INSTANTIATE_TEST_CASE_P( Tests,
                         TestLexerFails,
                         ::testing::ValuesIn( TestLexerFails::files ) );

TEST_P( TestLexerFails, FailsOnBadSamples )
{
    std::string filename = GetParam();
    std::cout << "Name:" << filename << std::endl;
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw CompilerException();
    }
    Compiler compiler( TCompilePhase::PHASE_LEX, input, stdout, false );

    testing::internal::CaptureStdout();
    EXPECT_THROW( compiler.Compile(), CompilerException );
    std::string output = testing::internal::GetCapturedStdout();
    CheckIfIdIsKeyword( output, "ID" );
    CheckIfIdIsKeyword( output, "NUM" );
    fclose( input );
}
