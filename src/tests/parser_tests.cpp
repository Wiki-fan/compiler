#include <fstream>
#include <regex>
#include "gtest/gtest.h"
#include "../compiler.h"
#include "../common.h"
#include "LineChecker.h"


class TestParserWorks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestParserWorks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java",
     "BadSamples/TC_1a.java", "BadSamples/TC_2a.java", "BadSamples/TC_3a.java", "BadSamples/TC_3b.java",
     "BadSamples/TC_4a.java", "BadSamples/TC_4b.java", "BadSamples/TC_5a.java", "BadSamples/TC_6a.java",
     "BadSamples/TC_7b.java", "BadSamples/TC_7c.java", "BadSamples/TC_8a.java", "BadSamples/TC_11a.java",
     "BadSamples/TC_12b.java", "BadSamples/TC_13a.java", "BadSamples/TC_13b.java", "BadSamples/TC_13c.java",
     "BadSamples/TC_bonus1.java"
    };

class TestParserFails : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string> TestParserFails::files =
    {
        "BadSamples/TC_9a.java",
        "BadSamples/TC_9a_2.java",
        "BadSamples/TC_13d(optional).java",
        "BadSamples/Synt_1_simple.java",
        "BadSamples/Synt_1_methods.java",
        "BadSamples/Synt_1_class.java",
        "BadSamples/Synt_1_goal.java",
        "BadSamples/Synt_1_statements.java",
        "BadSamples/Synt_1_expressions.java",
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestParserWorks,
                         ::testing::ValuesIn( TestParserWorks::files ) );

TEST_P( TestParserWorks, ParsesSamples )
{
    std::string filename = GetParam();
    //std::cout << "Name:" << filename << std::endl;
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : ParsesSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_PARSE, input, stdout, false );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        testing::internal::GetCapturedStdout();
        throw;
    }
    testing::internal::GetCapturedStdout();
    fclose( input );
}

INSTANTIATE_TEST_CASE_P( Tests,
                         TestParserFails,
                         ::testing::ValuesIn( TestParserFails::files ) );

TEST_P( TestParserFails, FailsOnBadSamples )
{
    std::string filename = GetParam();
    //std::cout << "Name:" << filename << std::endl;
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw CompilerException();
    }
    Compiler compiler( TCompilePhase::PHASE_PARSE, input, stdout, false );

    testing::internal::CaptureStdout();
    EXPECT_THROW( compiler.Compile(), CompilerException );
    compiler.PrintErrors();
    std::string output = testing::internal::GetCapturedStdout();

    std::vector<int> true_error_lines = get_true_error_lines( filename );
    std::vector<int> result_error_lines = get_result_error_lines( "\\[Line +(\\d+), column +(\\d+)\\]", output );

    ASSERT_EQ( true_error_lines, result_error_lines );

    fclose( input );
}
