#include <fstream>
#include "string_reformers.h"


std::string& ltrim( std::string& str )
{
    auto it2 = std::find_if( str.begin(),
                             str.end(),
                             []( char ch ) { return !std::isspace<char>( ch, std::locale::classic() ); } );
    str.erase( str.begin(), it2 );
    return str;
}

std::string& rtrim( std::string& str )
{
    auto it1 = std::find_if( str.rbegin(),
                             str.rend(),
                             []( char ch ) { return !std::isspace<char>( ch, std::locale::classic() ); } );
    str.erase( it1.base(), str.end() );
    return str;
}

std::string& trim( std::string& str )
{
    return rtrim( ltrim( str ) );
}

std::string shrink_spaces( const std::string& s )
{
    std::regex e( "\\s+" );
    return std::move( std::regex_replace( s, e, " " ) );

}

std::string rm_spaces_before( const std::string& s )
{
    std::regex e( " ([;(,)])" );
    return std::move( std::regex_replace( s, e, "$1" ) );
}

std::string rm_spaces_after( const std::string& s )
{
    std::regex e( "([(]) " );
    return std::move( std::regex_replace( s, e, "$1" ) );
}

std::string add_spaces_before( const std::string& s )
{
    std::regex e( "([\\{+\\-*<=])" );
    return std::move( std::regex_replace( s, e, " $1" ) );
}

std::string add_spaces_after( const std::string& s )
{
    std::regex e( R"(([\{,+\-*<=]))" );
    std::string tmp = std::regex_replace( s, e, "$1 " );
    std::regex e2( R"(&&)" );
    return std::move( std::regex_replace( tmp, e2, " && " ) );
}

std::string get_oneline( const std::string& filename )
{
    std::ifstream t( filename.c_str() );
    std::string line;
    std::string str;
    while( std::getline( t, line ) ) {
        std::size_t found = line.find( "//" );
        str += ' ';
        if( found != std::string::npos ) {
            str += line.substr( 0, found );
        } else {
            str += line;
        }
    }

    str = shrink_spaces( str );
    str = add_spaces_before( str );
    str = add_spaces_after( str );
    str = rm_spaces_before( str );
    str = rm_spaces_after( str );

    str = shrink_spaces( str );
    return trim( str );
}
