#include <fstream>
#include <sstream>
#include <istream>
#include <regex>
#include "gtest/gtest.h"
#include "../compiler.h"
#include "../common.h"
#include "LineChecker.h"


class TestSymbolTableWorks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestSymbolTableWorks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java"
    };

class TestSymbolTableFails : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string> TestSymbolTableFails::files =
    {
        "BadSamples/TC_1a.java", "BadSamples/TC_2a.java", "BadSamples/TC_3a.java", "BadSamples/TC_3b.java",
        "BadSamples/TC_4a.java", "BadSamples/TC_4b.java", "BadSamples/TC_5a.java", "BadSamples/TC_6a.java",
        "BadSamples/TC_7b.java", "BadSamples/TC_7c.java", "BadSamples/TC_8a.java", "BadSamples/TC_11a.java",
        "BadSamples/TC_12b.java", "BadSamples/TC_13a.java", "BadSamples/TC_13b.java", "BadSamples/TC_13c.java",
        "BadSamples/TC_bonus1.java"
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestSymbolTableWorks,
                         ::testing::ValuesIn( TestSymbolTableWorks::files ) );

TEST_P( TestSymbolTableWorks, ParsesSamples )
{
    std::string filename = GetParam();
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : ParsesSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_SYMBOLTABLE, input, stdout, true );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        testing::internal::GetCapturedStdout();
        throw;
    }
    testing::internal::GetCapturedStdout();

    fclose( input );
}

INSTANTIATE_TEST_CASE_P( Tests,
                         TestSymbolTableFails,
                         ::testing::ValuesIn( TestSymbolTableFails::files ) );

TEST_P( TestSymbolTableFails, FailsOnBadSamples )
{
    std::string filename = GetParam();
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw CompilerException();
    }
    Compiler compiler( TCompilePhase::PHASE_SYMBOLTABLE, input, stdout, false );

    testing::internal::CaptureStdout();
    EXPECT_THROW( compiler.Compile(), CompilerException );
    std::string output = testing::internal::GetCapturedStdout();

    std::vector<int> true_error_lines = get_true_error_lines( filename );
    std::vector<int> result_error_lines = get_result_error_lines( "\\(line +(\\d+)\\)", output );

    ASSERT_EQ( true_error_lines, result_error_lines );

    fclose( input );
}
